angular.module('App').controller('DashboardController', function ($rootScope, $scope, $http, $mdToast, $cookies, request) {

	var self = $scope;
	var root = $rootScope;

	root.closeAndDisableSearch();
	root.toolbar_menu = null;
	$rootScope.pagetitle = 'Dashboard';
    self.loading_all = false;
    self.data = {};

    self.refreshAll = function() {
        self.loading_all = true;
        request.refreshDashboard().then(function (resp) {
            location.reload();
        });
    }

    request.getDashboard().then(function (resp) {
        self.data = resp.data
    });

});
