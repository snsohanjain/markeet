<?php
require_once(realpath(dirname(__FILE__) . "/tools/rest.php"));

/*
 * This class handle all data display at dashboard
 */
class DASHBOARD extends REST{

    private $db = NULL;
    private $config = NULL;

    public function __construct($db) {
        parent::__construct();
        $this->db = $db;
        $this->config = new Config($this->db);
    }

	public function findAllPlain(){
		$query="SELECT * FROM dashboard d";
		return $this->db->get_list($query);
	}

    public function getAll() {
		$this->show_response($this->convertCodeValue($this->findAllPlain()));
    }
  
    protected function convertCodeValue($result) {
        $config = [];
        foreach ($result as $r) $config[$r['code']] = $r['value'];
        return $config;
    }

    public function updateAll(){
		if($this->get_request_method() != "GET") $this->response('',406);
        $query = "";
        $query = $query . "UPDATE dashboard SET value = (SELECT SUM(total_fees) FROM product_order WHERE payment_status = '') WHERE code = 'payment_waiting'; ";
        $query = $query . "UPDATE dashboard SET value = (SELECT SUM(total_fees) FROM product_order WHERE payment_status = 'PENDING') WHERE code = 'payment_pending'; ";
        $query = $query . "UPDATE dashboard SET value = (SELECT SUM(total_fees) FROM product_order WHERE payment_status = 'PAID') WHERE code = 'payment_paid'; ";
        $query = $query . "UPDATE dashboard SET value = (SELECT SUM(total_fees) FROM product_order WHERE payment_status = 'REFUND') WHERE code = 'payment_refund'; ";
        $query = $query . "UPDATE dashboard SET value = (SELECT SUM(total_fees) FROM product_order WHERE payment_status = 'DENIED') WHERE code = 'payment_denied'; ";
        $query = $query . "UPDATE dashboard SET value = (SELECT SUM(total_fees) FROM product_order WHERE payment_status = 'EXPIRED') WHERE code = 'payment_expired'; ";

        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM product_order WHERE status = 'WAITING') WHERE code = 'product_order_waiting'; ";
        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM product_order WHERE status = 'PROCESSED') WHERE code = 'product_order_processed'; ";
        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM product_order) WHERE code = 'product_order_total_order'; ";

        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM product WHERE draft = 0) WHERE code = 'product_published'; ";
        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM product WHERE draft = 1) WHERE code = 'product_draft'; ";
        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM product WHERE status = 'READY STOCK') WHERE code = 'product_ready_stock'; ";
        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM product WHERE status = 'OUT OF STOCK') WHERE code = 'product_out_of_stock'; ";
        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM product WHERE status = 'SUSPEND') WHERE code = 'product_suspend'; ";

        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM category WHERE draft = 0) WHERE code = 'category_published'; ";
        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM category WHERE draft = 1) WHERE code = 'category_draft'; ";

        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM news_info WHERE status = 'FEATURED') WHERE code = 'news_info_featured'; ";
        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM news_info WHERE draft = 0) WHERE code = 'news_info_published'; ";
        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM news_info WHERE draft = 1) WHERE code = 'news_info_draft'; ";

        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM app_version WHERE active = 1) WHERE code = 'app_version_active'; ";
        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM app_version WHERE active = 0) WHERE code = 'app_version_inactive'; ";

        $setting = $this->config->findByCodePlain('GENERAL');
        
        $query = $query . "UPDATE dashboard SET value = '" . $setting['currency'] . "' WHERE code = 'setting_currency'; ";
        $query = $query . "UPDATE dashboard SET value = '" . $setting['tax'] . "' WHERE code = 'setting_tax'; ";

        $query = $query . "UPDATE dashboard SET value = (SELECT count(id) FROM user) WHERE code = 'notification_user'; ";

        $query = $query . "UPDATE dashboard SET value = (SELECT CURRENT_TIMESTAMP) WHERE code = 'update'; ";
        
        $this->db->execute_multi_query($query);

        $resp = array("status" => "success", "msg" => "Success", "data" => null);
		$this->show_response($resp);
	}
}
?>