<?php

	$allowed_dir = array(
		'/uploads/product/',
		'/uploads/category/',
		'/uploads/news/',
	);

	$data = json_decode(file_get_contents("php://input"),true);

	$base_path 	= dirname(dirname(dirname(__FILE__)));
	$target_dir = $data['target_dir'];
	$file_names = $data['file_names'];

	// validation directory
	if(!in_array($target_dir, $allowed_dir)){
		print_exit("Invalid target dir");
	}
	
	// validation fila name
	foreach($file_names as $fn){
		if($fn != basename($fn)) print_exit("File name ". $fn);
	}

	foreach($file_names as $fn){
		$target_file = $base_path . $target_dir . $fn;
		if (file_exists($target_file)) {
			unlink($target_file);
		}
	}

	function print_exit($msg){
		echo $msg;
		exit;
	}

?>
